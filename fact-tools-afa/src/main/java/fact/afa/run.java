package fact.afa;

import stream.data.DataFactory;
import stream.data.TrackingDataFactory;

/**
 * This main executable is a thin wrapper for fact.run. It sets the default data
 * factory to an TrackingDataFactory instance.
 * 
 * @author bunse
 */
public class run {

	public static void main(String[] args) throws Exception {
		System.out.println("fact-tools-afa wrapper");
		DataFactory.setDefaultDataFactory(new TrackingDataFactory());
		fact.run.main(args);
	}

}
