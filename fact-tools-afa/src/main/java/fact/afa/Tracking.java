package fact.afa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.ProcessContext;
import stream.ProcessorList;
import stream.annotations.Parameter;
import stream.data.TrackingData;
import stream.data.TrackingDataFactory;

public class Tracking extends ProcessorList {
	
	static Logger log = LoggerFactory.getLogger(Tracking.class);

	@Parameter(required = true)
	public String id;

	@Override
	public void init(ProcessContext context) throws Exception {
		super.init(context);
		if (this.id == null)
			throw new Exception("Tracking processor has no ID yet");
	}

	@Override
	public Data process(Data data) {
		TrackingData tracking = TrackingDataFactory.cast(data);
		tracking.setCurrentCaller(this.id); // track
		data = super.process(tracking); // process
		if (data instanceof TrackingData)
			((TrackingData) data).setCurrentCaller(null);
		else
			log.warn(id + " received a non-TrackingData instance by an embedded processor");
		return data;
	}

}
