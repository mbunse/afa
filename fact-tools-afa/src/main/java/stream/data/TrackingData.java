package stream.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A Data item that tracks accesses to all of its keys.
 * 
 * @author bunse
 */
public class TrackingData extends DataImpl {
	
	public enum AccessType {
		GET,
		PUT
	}
	
	/**
	 * History items for tracking key accesses in a TrackingData item.
	 */
	public class HistoryItem implements Serializable {
		private static final long serialVersionUID = 4175727862161096139L;
		private String key;
		private String caller;
		private AccessType type;
		private long nanoSeconds;
		public HistoryItem(String key, String caller, AccessType type, long nanoSeconds) {
			this.key = key;
			this.caller = caller;
			this.type = type;
			this.nanoSeconds = nanoSeconds;
		}
		public String getKey() {
			return key;
		}
		public String getCaller() {
			return caller;
		}
		public AccessType getType() {
			return type;
		}
		public long getNanoSeconds() {
			return nanoSeconds;
		}
	}

	/** The unique class ID */
	private static final long serialVersionUID = -3437912893224129268L;
	
	public static final String TRACKING_KEY = "@history";
	
	private String currentCaller;

	@SuppressWarnings("deprecation")
	protected TrackingData(Map<String, Serializable> data) {
		super(data); // only deprecated to promote DataFactory
		if (!data.containsKey(TRACKING_KEY))
			for (String key : data.keySet()) // start writing history
				this.trackAccess(key, AccessType.PUT);
	}

	protected TrackingData() {}

	/** Append a HistoryItem to the TRACKING_KEY list. */
	private void trackAccess(String key, AccessType type) {
		if (!TRACKING_KEY.equals(key) // do not track the TRACKING_KEY
				&& this.currentCaller != null) {
			long nanoSeconds = 0; // TODO specify nanoSeconds
			this.getHistory().add(new HistoryItem(key, this.currentCaller, type, nanoSeconds));
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<HistoryItem> getHistory() {
		if (!this.containsKey(TRACKING_KEY))
			this.put(TRACKING_KEY, new ArrayList<HistoryItem>());
		return (List<HistoryItem>) this.get(TRACKING_KEY);
	}

	@Override
	public Serializable get(Object key) {
		Serializable result = super.get(key);
		if (result != null) // only track GET accesses to keys that exist
			this.trackAccess((String) key, AccessType.GET);
		return result;
	}

	@Override
	public Serializable put(String key, Serializable value) {
		if (value != null
				&& !value.equals(this.get(key))) // only track PUT accesses that make a change
			this.trackAccess((String) key, AccessType.PUT);
		return super.put(key, value);
	}

	public String getCurrentCaller() {
		return currentCaller;
	}

	public void setCurrentCaller(String currentCaller) {
		this.currentCaller = currentCaller;
	}

}
