package stream.data;

import java.io.Serializable;
import java.util.Map;

import stream.Data;

public class TrackingDataFactory extends DataFactory {

	@Override
	public Data createDataItem() {
		return new TrackingData();
	}

	@Override
	public Data createDataItem(Map<String, Serializable> item) {
		return new TrackingData(item);
	}

	public static TrackingData cast(Data data) {
		if (data instanceof TrackingData)
			return (TrackingData) data;
		else
			return (TrackingData) TrackingDataFactory.create(data);
	}

}
