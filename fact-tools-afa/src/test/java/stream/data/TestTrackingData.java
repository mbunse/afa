package stream.data;

import static org.junit.jupiter.api.Assertions.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fact.afa.Tracking;
import stream.Data;
import stream.Processor;

class TestTrackingData {
	
	private static TrackingDataFactory FACTORY;
	
	/** A processor for testing TrackingData items */
	private static class TestingProcessor implements Processor {

		private boolean put; // whether to perform a PUT access

		public TestingProcessor(boolean put) {
			this.put = put;
		}

		@Override
		public Data process(Data data) {
			if (this.put)
				data.put("Foo", "Bar");
			data.get("Foo");
			return data;
		}

	} // TestingProcessor

	private static Processor createProcessor(boolean put) {
		TestingProcessor testingProcessor = new TestingProcessor(put);
		Tracking tracking = new Tracking();
		tracking.id = "test";
		tracking.add(testingProcessor);
		return tracking;
	}

	@BeforeEach
	void beforeEach() {
		FACTORY = new TrackingDataFactory();
	}

	@Test
	void testEmpty() {
		Data data = FACTORY.createDataItem();
		assertFalse(data.containsKey("@history"));
	}

	@Test
	void testPut() {
		Data data = FACTORY.createDataItem();
		Processor testingProcessor = createProcessor(true);
		testingProcessor.process(data);
		checkSimpleHistory(data, true);
	}

	@Test
	void testClone() {
		Data original = FACTORY.createDataItem();
		Processor testingProcessor = createProcessor(true);
		testingProcessor.process(original);
		Data clone = FACTORY.createDataItem(original);
		checkSimpleHistory(clone, true);
	}

	@Test
	void testMap() {
		Map<String, Serializable> original = new HashMap<>();
		original.put("Foo", "Bar");
		Data clone = FACTORY.createDataItem(original);
		Processor testingProcessor = createProcessor(false);
		testingProcessor.process(clone);
		clone.get("Foo");
		checkSimpleHistory(clone, false);
	}
	
	@SuppressWarnings("unchecked")
	private void checkSimpleHistory(Data data, boolean testPutProcessor) {
		assertTrue(data.containsKey("@history"));
		List<TrackingData.HistoryItem> history = (List<TrackingData.HistoryItem>) data.get("@history");
		assertEquals(history.size(), testPutProcessor ? 2 : 1);
		if (testPutProcessor) {
			assertEquals(history.get(0).getKey(), "Foo");
			assertEquals(history.get(0).getType(), TrackingData.AccessType.PUT);
			assertEquals(history.get(0).getCaller(), "test");
		}
		int i = testPutProcessor ? 1 : 0;
		assertEquals(history.get(i).getKey(), "Foo");
		assertEquals(history.get(i).getType(), TrackingData.AccessType.GET);
		assertEquals(history.get(i).getCaller(), "test");
	}

}
