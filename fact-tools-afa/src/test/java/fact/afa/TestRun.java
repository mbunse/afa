package fact.afa;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import stream.data.DataFactory;
import stream.data.TrackingData;
import stream.data.TrackingDataFactory;

class TestRun {

	@BeforeAll
	public static void beforeAll() throws Exception {
		DataFactory.setDefaultDataFactory(new TrackingDataFactory());
	}
	
	@Test
	public void testSetDefaultDataFactory() {
		assertTrue(DataFactory.create() instanceof TrackingData);
	}
}
