# dependency graph of Processors
afa.pdf: venv/bin/python3 plot-dependency-graph.py afa.json
	$^ $@
afa.json: fact-tools-afa/target/fact-tools-afa-0.0.1-SNAPSHOT.jar fact-tools-afa/src/main/resources/afa/stdAnalysis.xml
	java -jar $^

removal-tests: $(patsubst fact-tools-afa/src/test/resources/%.xml,.REMOVAL_TESTS/%.json,$(wildcard fact-tools-afa/src/test/resources/*.xml))
.REMOVAL_TESTS/%.json: fact-tools-afa/target/fact-tools-afa-0.0.1-SNAPSHOT.jar fact-tools-afa/src/test/resources/%.xml
	java -jar $^

# compile a JAR archive with maven
fact-tools-afa/target/fact-tools-afa-0.0.1-SNAPSHOT.jar: $(shell find fact-tools-afa/src -type f) fact-tools-afa/pom.xml lib/.INSTALL fact-tools-afa/src/main/resources/aux/2013/01/02/20130102.DRIVE_CONTROL_SOURCE_POSITION.fits
	cd fact-tools-afa && mvn package

# retrieve AUX files
fact-tools-afa/src/main/resources/aux/2013/01/02/20130102.DRIVE_CONTROL_SOURCE_POSITION.fits: lib/fact-tools-v1.1.3.jar
	unzip -o $< "aux/**/*" -d fact-tools-afa/src/main/resources/ && touch $@

# install maven dependencies
lib/.INSTALL: lib/fact-tools-v1.1.3.jar
	mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -Dfile=$< && touch $@
lib/fact-tools-v1.1.3.jar:
	wget -O $@ https://github.com/fact-project/fact-tools/releases/download/v1.1.3/fact-tools-v1.1.3.jar

# python dependencies
venv/.REQUIREMENTS: requirements.txt venv/bin/pip
	venv/bin/pip install -r $< && touch $@
venv/bin/pip:
	python3 -m venv venv

.PHONY: removal-tests
