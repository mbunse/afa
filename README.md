# Active feature acquisition with fact-tools

Can we reduce the computational cost of the feature extraction pipeline by omitting costly features?

## Feature dependency graph

You need to have Java, Maven, and Python3 installed on your system. A plot of feature dependencies is then generated by calling

```bash
make
```

at the root directory of this repository. Consider calling `make -n` to inspect the build steps before executing them.

## fact-tools-afa

This Maven project extends the [fact-tools](https://github.com/fact-project/fact-tools) package with which physicists of the FACT collaboration extract features from the raw telescope recordings. The extension features an additional processor `afa.Tracking` which tracks read and write accesses to the data instances. From these tracked access histories we can later derive dependencies between features.

## Open issues

- we currently track only a test run, see `fact-tools-afa/src/main/resources/afa/stdAnalysis.xml` but a real analysis with real data would be needed
- we do not yet extract timing information and therefore do not know the cost of each feature
- once the above issues are resolved, we aim for a cost model `c: F -> R` which provides the computational cost for any feature subset `F`.
