import click
import json
import matplotlib.pyplot as plt
import networkx as nx
import pandas as pd

DL2 = [
    "concentration_cog",
    "concentration_core",
    "concentration_one_pixel",
    "concentration_two_pixel",
    "leakage1",
    "leakage2",
    "size",
    "width",
    "length",
    "skewness_long",
    "skewness_trans",
    "kurtosis_long",
    "kurtosis_trans",
    "num_islands",
    "num_pixel_in_shower",
    "photoncharge_shower_mean",
    "photoncharge_shower_variance",
    "photoncharge_shower_max",
    "slope_long",
    "time_gradient_slope_long"
]

DL3 = [
    "ceres_event_event_reuse",
    "corsika_event_header_event_number",
    "corsika_event_header_first_interaction_height",
    "corsika_event_header_num_reuse",
    "corsika_event_header_total_energy",
    "corsika_event_header_x",
    "corsika_event_header_y",
    "corsika_run_header_run_number",
    "event_num",
    "index",
    "pointing_position_az",
    "pointing_position_zd",
    "run_id",
    "source_position_az",
    "source_position_zd",
    "theta_deg",
    "theta_deg_off_1",
    "theta_deg_off_2",
    "theta_deg_off_3",
    "theta_deg_off_4",
    "theta_deg_off_5",
    "true_disp"
]

def find_last_put_caller(key, history):
    """Find the caller of the last PUT access of the key."""
    for history_i in reversed(history):
        if history_i["key"] == key and history_i["type"] == "PUT":
            return history_i["caller"]
    return "<INIT>"

def to_edgelist(json_item):
    """Convert a JSON data item to a networkx edgelist DataFrame."""
    history = json_item["@history"]
    for key in DL2:
        history.append({"key": key, "type": "GET", "caller": "<DL2>"})
    for key in DL3:
        history.append({"key": key, "type": "GET", "caller": "<DL3>"})
    df_data = [] # tuples of "source" and "target"
    for (i, history_i) in enumerate(history):
        if history_i["type"] != "GET":
            continue # skip everything that is not a GET access
        target = history_i["caller"]
        source = find_last_put_caller(history_i["key"], history[:i])
        if source != target:
            df_data.append([source, target, history_i["key"]])
    return pd.DataFrame(df_data, columns=["source", "target", "key"])

@click.command()
@click.argument('input_json', type=click.Path(exists=True))
@click.argument('output_image', type=click.Path())
@click.option('--seed', type=int, default=876)
def main(input_json, output_image, seed):
    with open(input_json, "rt") as f:
        json_data = json.load(f)
    G = nx.from_pandas_edgelist(to_edgelist(json_data[0]), edge_attr=True)
    
    pos = nx.spring_layout(G, seed=seed)
    nx.draw_networkx_nodes(G, pos, node_size=1500)
    nx.draw_networkx_labels(G, pos)
    nx.draw_networkx_edges(G, pos, arrows=True, min_source_margin=25, min_target_margin=25)
    edge_labels = dict([((n1, n2), d["key"]) for (n1, n2, d) in G.edges(data=True)])
    nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels)
    plt.gcf().set_size_inches(24, 18)
    plt.box(False)
    plt.savefig(output_image)

if __name__ == "__main__":
    main()
